FROM registry.sindominio.net/debian

RUN apt -y update && \
    apt install -y mumble-server

COPY entrypoint.sh entrypoint.sh

RUN chmod 755 /entrypoint.sh && \
    mkdir -p /mumble

VOLUME ["/mumble"]

ENTRYPOINT [ "/bin/sh", "/entrypoint.sh" ] 
