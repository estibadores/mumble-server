#!/bin/bash

mkdir -p /mumble/db

[ -f /mumble/db/murmur.sqlite ] || touch /mumble/db/murmur.sqlite 

exec /usr/sbin/murmurd -fg -ini /mumble/config.ini
