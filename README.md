# Mumble Server

Use GNU/Linux Debian Murmur


## Configure 

Copy _ENV_ variables

```
$ cp .env.sample .env
```

Check variables and create $MUMBLE_DATA_PATH

Example: ./data

```
$ mkdir data
```

Add owner/privileges to $MUMBLE_USER_GROUP

Example: 1000:1000

```
$ chown 1000:1000 data
```

Up container

```
$ docker-compose pull
$ docker-compose up -d
```

Enjoy ;)
